﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI;

namespace SlotMachine.ViewModels
{
    public class MachineViewModel
    {
        public int Credits { get; set; }
        public int OldCredits { get; set; }
        public int BetSize { get; set; }
        public string Spinner1 { get; set; }
        public string Spinner2 { get; set; }
        public string Spinner3 { get; set; }

        //Implement full spinning
        //public List<System.Web.UI.WebControls.Image> Spinner1Images { get; set; }
        //public List<System.Web.UI.WebControls.Image> Spinner2Images { get; set; }
        //public List<System.Web.UI.WebControls.Image> Spinner3Images { get; set; }

        public string Activity { get; set; }
        public int AmountWonLastSpin { get; set; }
    }
}
