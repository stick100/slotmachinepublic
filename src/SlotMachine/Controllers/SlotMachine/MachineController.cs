﻿using Microsoft.AspNet.Mvc;
using SlotMachine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SlotMachine.Controllers.SlotMachine
{
    public class MachineController : Controller
    {
        //Enum this 
        const string _1BarUrl = "~/Images/1Bar.png";
        const string _2BarUrl = "~/Images/2Bar.png";
        const string _3BarUrl = "~/Images/3Bar.png";
        const string _7Url = "~/Images/7.png";
        const string _BlankUrl = "~/Images/Blank.png";
        const string _CherryUrl = "~/Images/Cherry.png";
        const int _dontRevealSpinners = -1;
        const int _gamePaidOutAndDisabled = -2;

        public IActionResult Index()
        {
            MachineViewModel model = new MachineViewModel();
            model.Credits = 100;
            model.OldCredits = model.Credits;
            model.BetSize = 0;

            //Build all three spinners
            //model.Spinner1Images.Add

            Random rand = new Random((int)DateTime.Now.Ticks);

            model.Spinner1 = GetImageForSpinnerResult(rand);
            model.Spinner2 = GetImageForSpinnerResult(rand);
            model.Spinner3 = GetImageForSpinnerResult(rand);
            model.AmountWonLastSpin = _dontRevealSpinners;

            var view = View();
            view.ViewData.Model = model;
            return view;
        }

        private static string GetImageForSpinnerResult(Random rand)
        {
            List<System.Web.UI.WebControls.Image> imagesToBuild = new List<System.Web.UI.WebControls.Image>();
            List<String> urlsToMakeIntoImages = new List<string>();
            urlsToMakeIntoImages.Add(_7Url);
            urlsToMakeIntoImages.Add(_3BarUrl);
            urlsToMakeIntoImages.Add(_2BarUrl);
            urlsToMakeIntoImages.Add(_2BarUrl);
            urlsToMakeIntoImages.Add(_1BarUrl);
            urlsToMakeIntoImages.Add(_1BarUrl);
            urlsToMakeIntoImages.Add(_1BarUrl);
            urlsToMakeIntoImages.Add(_CherryUrl);
            for (int i = 0; i < 9; i++)
            {
                urlsToMakeIntoImages.Add(_BlankUrl);
            }

            return urlsToMakeIntoImages[rand.Next(0, urlsToMakeIntoImages.Count)]; ;
        }

        private static List<System.Web.UI.WebControls.Image> BuildImages(Random rand)
        {
            List<System.Web.UI.WebControls.Image> imagesToBuild = new List<System.Web.UI.WebControls.Image>();
            List<String> urlsToMakeIntoImages = new List<string>();
            urlsToMakeIntoImages.Add(_7Url);
            urlsToMakeIntoImages.Add(_3BarUrl);
            urlsToMakeIntoImages.Add(_2BarUrl);
            urlsToMakeIntoImages.Add(_2BarUrl);
            urlsToMakeIntoImages.Add(_1BarUrl);
            urlsToMakeIntoImages.Add(_1BarUrl);
            urlsToMakeIntoImages.Add(_1BarUrl);
            urlsToMakeIntoImages.Add(_CherryUrl);
            for (int i = 0; i < 9; i++)
            {
                urlsToMakeIntoImages.Add(_BlankUrl);
            }
            while (urlsToMakeIntoImages.Count > 0)
            {
                System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();
                int indexToAdd = rand.Next(0, urlsToMakeIntoImages.Count);
                image.ImageUrl = urlsToMakeIntoImages[indexToAdd];
                urlsToMakeIntoImages.RemoveAt(indexToAdd);
                imagesToBuild.Add(image);
            }

            return imagesToBuild;
        }

        [HttpPost]
        public IActionResult Index(MachineViewModel model)
        {
            if (model.Activity == "Bet1More")
            {
                return Bet1More(model);
            }
            else if (model.Activity == "MaxBetAndSpin")
            {
                return MaxBetAndSpin(model);
            }
            else if (model.Activity == "Spin")
            {
                return Spin(model);
            }
            else if (model.Activity == "Payout")
            {
                return Payout(model);
            }
            else if (model.Activity == "Reset")
            {
                return Reset(model);
            }
            return View();

        }

        private IActionResult Payout(MachineViewModel model)
        {
            
            model.BetSize = 0;
            model.Credits = 100;
            model.OldCredits = model.Credits;//Resets, but can't play yet
            model.AmountWonLastSpin = _gamePaidOutAndDisabled;//Leaves everything disabled

            var view = View();
            view.ViewData.Model = model;
            return view;
        }

        private IActionResult Reset(MachineViewModel model)
        {
            model.BetSize = 0;
            model.Credits = 100;
            model.OldCredits = model.Credits;//Resets, ready
            model.AmountWonLastSpin = _dontRevealSpinners;

            var view = View();
            view.ViewData.Model = model;
            return view;
        }

        private IActionResult Spin(MachineViewModel model)
        {
            //Do spinning
            
            //Build all three spinners
            //model.Spinner1Images.Add

            Random rand = new Random((int)DateTime.Now.Ticks);

            model.Credits -= model.BetSize;
            model.OldCredits = model.Credits;

            model.Spinner1 = GetImageForSpinnerResult(rand);
            model.Spinner2 = GetImageForSpinnerResult(rand);
            model.Spinner3 = GetImageForSpinnerResult(rand);
            //Get new results, now lets calc
            model = DetermineOutcome(model);

            model.BetSize = 0;

            var view = View();
            view.ViewData.Model = model;
            return view;
        }

        private MachineViewModel DetermineOutcome(MachineViewModel model)
        {
            if (model.Spinner1 == _7Url && model.Spinner2 == _7Url && model.Spinner3 == _7Url)
            {
                if (model.BetSize == 3)//Special case
                {
                    model.AmountWonLastSpin = 1500;
                    model.Credits += model.AmountWonLastSpin;
                }
                else
                {
                    model.AmountWonLastSpin = 300 * model.BetSize;
                    model.Credits += model.AmountWonLastSpin;
                }
            }
            else if (model.Spinner1 == _3BarUrl && model.Spinner2 == _3BarUrl && model.Spinner3 == _3BarUrl)
            {
                model.AmountWonLastSpin = 100 * model.BetSize;
                model.Credits += model.AmountWonLastSpin;
            }
            else if (model.Spinner1 == _2BarUrl && model.Spinner2 == _2BarUrl && model.Spinner3 == _2BarUrl)
            {
                    model.AmountWonLastSpin = 50 * model.BetSize;
                    model.Credits += model.AmountWonLastSpin;

            }
            else if (model.Spinner1 == _1BarUrl && model.Spinner2 == _1BarUrl && model.Spinner3 == _1BarUrl)
            {
                    model.AmountWonLastSpin = 25 * model.BetSize;
                    model.Credits += model.AmountWonLastSpin;
            }
            else if ((model.Spinner1 == _1BarUrl || model.Spinner1 == _2BarUrl || model.Spinner1 == _3BarUrl)
                && (model.Spinner2 == _1BarUrl || model.Spinner2 == _2BarUrl || model.Spinner2 == _3BarUrl)
                && (model.Spinner3 == _1BarUrl || model.Spinner3 == _2BarUrl || model.Spinner3 == _3BarUrl))
            {

                model.AmountWonLastSpin = 5 * model.BetSize;
                model.Credits += model.AmountWonLastSpin;
            }
            if (model.Spinner1 == _CherryUrl)
            {
                model.AmountWonLastSpin += 2 * model.BetSize;
                model.Credits += 2 * model.BetSize;
            }
            if (model.Spinner2 == _CherryUrl)
            {
                model.AmountWonLastSpin += 2 * model.BetSize;
                model.Credits += 2 * model.BetSize;
            }
            if (model.Spinner3 == _CherryUrl)
            {
                model.AmountWonLastSpin += 2 * model.BetSize;
                model.Credits += 2 * model.BetSize;
            }


            return model;
        }

        private IActionResult Bet1More(MachineViewModel model)
        {
            if (model.BetSize < 3 && model.BetSize < model.Credits)
                model.BetSize++;
            model.AmountWonLastSpin = _dontRevealSpinners;
            model.OldCredits = model.Credits;
            var view = View();
            view.ViewData.Model = model;
            return view;
        }

        private IActionResult MaxBetAndSpin(MachineViewModel model)
        {
            model.BetSize = Math.Min(3, model.Credits);
            //Spin somehow
            return Spin(model);
        }
    }
}
